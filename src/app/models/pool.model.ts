import { Player } from "./player.model";
import { Match } from "./match.model";

export class Pool{
    players: Player[] = new Array();
    matches: Match[] = new Array();
}