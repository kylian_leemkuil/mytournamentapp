export class Player{
    static _id: number = 0;
    public id: number;
    playerName: string;
    poolPoints: number;
    constructor (playerName: string, poolPoints?: number){
        this.playerName = playerName;
        this.poolPoints = 0;
        this.id = Player._id++;
     }
}