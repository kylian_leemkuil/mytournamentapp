import { Player } from "./player.model";

export class Match{
    playerOne: Player;
    playerTwo: Player;
    playerOneScore: number;
    playerTwoScore: number;
    result?: MatchStatus;
    constructor (playerOne: Player, playerTwo: Player){
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.playerOneScore = 0;
        this.playerTwoScore = 0;
     }
}