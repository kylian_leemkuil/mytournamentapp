import { Player } from "./player.model";

export class Bracket{
    quaterFinals: Player[] = new Array();
    semiFinals: Player[] = new Array();
    finals: Player[] = new Array();
}