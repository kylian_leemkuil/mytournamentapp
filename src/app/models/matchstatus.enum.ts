enum MatchStatus {
    NotFinished,
    InProgress,
    Draw,
    PlayerOneWin,
    PlayerTwoWin
}