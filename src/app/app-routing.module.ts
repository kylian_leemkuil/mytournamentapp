import { NgModule } from '@angular/core';
import { Routes, Router, RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';
import { TournamentComponent } from './tournament/tournament.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/start' },
    { path: 'tournament', component: TournamentComponent },
    { path: '**', component: StartComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
