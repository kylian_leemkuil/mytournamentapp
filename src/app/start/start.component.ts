import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Player } from '../models/player.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Pool } from '../models/pool.model';
import { Match } from '../models/match.model';
import { TournamentService } from '../services/tournament.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {
  players: Player[] = new Array();
  pools: Pool[] = new Array();
  constructor(private router: Router, public snackBar: MatSnackBar, private tournamentService: TournamentService) { }

  ngOnInit() {
    if(this.tournamentService.havePools()){
      this.router.navigate(['/tournament']);
    }
  }

  addName(newPlayerName): void {
    if(!this.checkNameExist(newPlayerName.value)){
      if(newPlayerName.value){
        this.players.push(new Player(newPlayerName.value));
        newPlayerName.value = null;
      } else {
        this.snackBar.open("Player name can't be empty", "Okay", {
          duration: 2000,
        });
      }
    } else {
      this.snackBar.open("Player name already exists", "Okay", {
        duration: 2000,
      });
    }
  }

  checkNameExist(name: string): boolean {
    for (let player of this.players) {
      if(name === player.playerName){
        return true;
      }
    }
    return false;
  }

  deletePlayer(id: number): void {
    this.players.splice(id, 1);
  }

  startTournament(): void {
    if(this.players.length > 1){
      //determine pools
      this.determinePools();
      //determine matches
      this.determinePoolMatches();
      //Add pools to localstorage
      this.tournamentService.setPools(this.pools);
      this.router.navigate(['/tournament']);
    } else {
      this.snackBar.open("Tournament need aleast 2 players", "Okay", {
        duration: 2000,
      });
    }
  }

  determinePools(): void {
    let amountPlayers = this.players.length;
    let amountPools = 0;
    let poolSize = 0;
    
    for(let i = 3;i < 6; i++){
      if(amountPlayers % i == 0){
        amountPools = amountPlayers / i;
        poolSize = i;
      }
    }
    if(amountPools == 0){
      for(let i = 3;i < 6; i++){
        if(amountPlayers % i > i - 2){
          amountPools = Math.floor(amountPlayers / i) + 1;
          poolSize = i;
        }
      }
    }
    for(let i = 0; i < amountPools; i++){
      this.pools.push(new Pool());
      for(let j = 0; j < poolSize; j++){
        if(this.players.length != 0){
          let randomNumber = this.randomIntFromInterval(0,this.players.length -1);
          let randomPlayer = this.players[randomNumber];
          this.players.splice(randomNumber, 1);
          this.pools[i].players.push(randomPlayer);
        }
      }
    } 
  }

  determinePoolMatches(): void {
    this.pools.forEach(pool => {
      let tempPlayers = pool.players.map(x => Object.assign({}, x)); 
      for(let i = tempPlayers.length - 1; i >= 0; i--){
        for(let j = tempPlayers.length - 1; j >= 0; j--){
          if(tempPlayers[i] != tempPlayers[j]){
            let m = new Match(tempPlayers[i],tempPlayers[j]);
            pool.matches.push(m);
          }
        }
        tempPlayers.splice(i,1);
      }
    });
  }

  randomIntFromInterval(min,max): number {
    return Math.floor(Math.random()*(max-min+1)+min);
  }

}
