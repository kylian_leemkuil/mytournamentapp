import { Injectable } from '@angular/core';
import { Pool } from '../models/pool.model';
import { Bracket } from '../models/bracket.model';

@Injectable({
  providedIn: 'root'
})
export class TournamentService {

  constructor() { }

  getPools(): Pool[] {
    return JSON.parse(localStorage.getItem('Pools'));
  }

  setPools(pools: Pool[]): void{
    localStorage.setItem('Pools', JSON.stringify(pools));
  }

  deletePools(): void {
    localStorage.removeItem('Pools');
  }

  havePools(): boolean {
    if(localStorage.getItem('Pools')){
      return true;
    }
    return false;
  }

  setBracket(bracket: Bracket): void {
    localStorage.setItem('Bracket', JSON.stringify(bracket));
  }

  getBracket(): Bracket {
    return JSON.parse(localStorage.getItem('Bracket'));
  }

  haveBracket(): boolean {
    if(localStorage.getItem('Bracket')){
      return true;
    }
    return false;
  }

  deleteBracket(): void {
    localStorage.removeItem('Bracket');
  }

  getWinner(): string {
    return localStorage.getItem('Winner');
  }

  setWinner(winner: string): void {
    localStorage.setItem('Winner', winner);
  }

  deleteWinner(): void {
    localStorage.removeItem('Winner');
  }
}
