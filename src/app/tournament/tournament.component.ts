import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Pool } from '../models/pool.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Player } from '../models/player.model';
import { Bracket } from '../models/bracket.model';
import { TournamentService } from '../services/tournament.service';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.css']
})
export class TournamentComponent implements OnInit {
  amountPools: number = 0;
  pools: Pool[] = new Array();
  poolWinners: Player[] = new Array();
  bracket: Bracket = new Bracket;
  poolsFinished: boolean = false;
  winnerName: string;
  
  constructor(private router: Router, public snackBar: MatSnackBar, private tournamentService: TournamentService) { }

  ngOnInit() {
    //Get localstorage data
    this.pools = this.tournamentService.getPools();
    if(this.tournamentService.haveBracket()){
      this.poolsFinished = true;
      this.bracket = this.tournamentService.getBracket();
      this.winnerName = this.tournamentService.getWinner();
    }
  }

  newTournament(): void {
    this.tournamentService.deletePools();
    this.tournamentService.deleteBracket();
    this.tournamentService.deleteWinner();
    this.router.navigate(['/start']);
  }

  saveMatch(poolNr:number, matchNr: number, playerOne: HTMLInputElement, playerTwo: HTMLInputElement, saveButton: HTMLButtonElement): void {
    let playerOnePoints = 0;
    let playerTwoPoints = 0;
    const playerOneScore =  playerOne.value;
    const playerTwoScore =  playerTwo.value;
    if(playerOneScore > playerTwoScore){
      playerOnePoints = 3;
    } else if(playerOneScore < playerTwoScore) {
      playerTwoPoints = 3;
    } else {
      playerOnePoints = 1;
      playerTwoPoints = 1;
    }
    this.pools[poolNr].matches[matchNr].playerOneScore = +playerOneScore;
    this.pools[poolNr].matches[matchNr].playerTwoScore = +playerTwoScore;
    this.pools[poolNr].players.find(e => e.id == this.pools[poolNr].matches[matchNr].playerOne.id).poolPoints += playerOnePoints;
    this.pools[poolNr].players.find(e => e.id == this.pools[poolNr].matches[matchNr].playerTwo.id).poolPoints += playerTwoPoints;
    this.pools[poolNr].matches[matchNr].playerOne.poolPoints = playerOnePoints;
    this.pools[poolNr].matches[matchNr].playerTwo.poolPoints = playerTwoPoints;
    saveButton.disabled = true;
    this.tournamentService.setPools(this.pools);
  }

  finish(){
    if(this.checkAllMatchesPlayed()){
      this.fillWinners();
      this.poolsFinished = true;
      this.fillBracket();
      this.clearPoolPoints(this.bracket.finals);
      this.clearPoolPoints(this.bracket.semiFinals);
      this.clearPoolPoints(this.bracket.quaterFinals);
      this.tournamentService.setBracket(this.bracket);
    } else {
      this.snackBar.open("Not all pools are finished yet!", "Okay", {
        duration: 2000,
      });3
    }
  }

  fillWinners() {
    this.pools.forEach(p => {
      let highestPoints = Math.max.apply(Math, p.players.map((o) => { return o.poolPoints; }))
      p.players.forEach(pl => {
        if(pl.poolPoints == highestPoints){
          this.poolWinners.push(pl);
        }
      });
    });
  }

  fillBracket() {
    if(this.poolWinners.length == 1){
      this.winnerName = this.poolWinners[0].playerName;
      this.tournamentService.setWinner(this.winnerName);
    } else if(this.poolWinners.length == 2) {
      this.bracket.finals = this.poolWinners;
    } else if(this.poolWinners.length > 2 && this.poolWinners.length < 5) {
      this.bracket.semiFinals = this.poolWinners;
    } else {
      this.bracket.quaterFinals = this.poolWinners;
    }
  }

  checkAllMatchesPlayed(): boolean {
    let poolsPlayed = true;
    this.pools.forEach(p => {
      p.matches.forEach(m => {
        if(m.playerTwoScore == 0 && m.playerOneScore == 0){
          poolsPlayed = false;
        } 
      });
    });
    return poolsPlayed;
  }

  checkButtonSemiFinals(): boolean {
    if(this.bracket.quaterFinals.length > 0 && this.bracket.semiFinals.length == 0){
      return true;
    }
    return false;
  }

  checkButtonFinals(): boolean {
    if(this.bracket.semiFinals.length > 0 && this.bracket.finals.length == 0){
      return true;
    }
    return false;
  }

  checkButtonWinner(): boolean {
    if(this.bracket.finals.length > 0){
      return true;
    }
    return false;
  }

  checkWinner() {
    this.tournamentService.setBracket(this.bracket);
    if(this.bracket.finals[0].poolPoints > this.bracket.finals[1].poolPoints){
      this.winnerName = this.bracket.finals[0].playerName;
    } else {
      this.winnerName = this.bracket.finals[1].playerName;
    }
    this.tournamentService.setWinner(this.winnerName);
  }

  startSemiFinals(): void{
    for(let i = 0; i < 8; i = i + 2){
      if(i == 6 && !this.bracket.quaterFinals[7]){
        this.bracket.semiFinals.push(this.bracket.quaterFinals[6]);
        break;
      } 
      if(this.bracket.quaterFinals[i].poolPoints > this.bracket.quaterFinals[i + 1].poolPoints){
        this.bracket.semiFinals.push(JSON.parse(JSON.stringify(this.bracket.quaterFinals[i])));
      } else {
        this.bracket.semiFinals.push(JSON.parse(JSON.stringify(this.bracket.quaterFinals[i + 1])));
      }
    }
    this.clearPoolPoints(this.bracket.semiFinals);
    this.tournamentService.setBracket(this.bracket);
  }

  startFinals(): void{
    for(let i = 0; i < 4; i = i + 2){
      if(i == 2 && !this.bracket.semiFinals[3]){
        this.bracket.finals.push(this.bracket.semiFinals[2]);
        break;
      } 
      if(this.bracket.semiFinals[i].poolPoints > this.bracket.semiFinals[i + 1].poolPoints){
        this.bracket.finals.push(JSON.parse(JSON.stringify(this.bracket.semiFinals[i])));
      } else {
        this.bracket.finals.push(JSON.parse(JSON.stringify(this.bracket.semiFinals[i + 1])));
      }
    }
    this.clearPoolPoints(this.bracket.finals);
    this.tournamentService.setBracket(this.bracket);
  }

  clearPoolPoints(array) {
      array.forEach(e => {
        if(e){
          e.poolPoints = 0;
        }
      });
  }
}
