# MyTournamentApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Clone repository
```sh
git clone https://kylian_leemkuil@bitbucket.org/kylian_leemkuil/mytournamentapp.git
```

## Development
1. Run the command below
```sh 
npm install
```
2. Run the command below
```sh 
ng serve
```
3. The server is now running on http://localhost:4200

## Build
1. Run the command below
```sh 
npm install
```
2. Run the command below
```sh 
ng build --prod
```
3. Download the "dist" folder in the map and copy it to your web-server thats all you need.